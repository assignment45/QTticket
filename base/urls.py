from django.urls import path
from . import views

## urls dictionary for each view ##
urlpatterns = [
    path('login/', views.loginView, name="login"),
    path('logout/', views.logoutView, name="logout"),
    path('register/', views.registerView, name="register"),

    path('', views.home, name="home"),
    path('profile/<str:pk>', views.profile, name="profile"),
    path('ticket/<str:pk>', views.ticket, name="ticket"),

    path('create-ticket/', views.createTicket, name="create-ticket"),
    path('update-ticket/<str:pk>', views.updateTicket, name="update-ticket"),
    path('resolve-ticket/<str:pk>', views.resolveTicket, name="resolve-ticket"),
    path('delete-comment/<str:pk>', views.deleteComment, name="delete-comment"),
]