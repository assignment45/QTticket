from django.contrib import admin

from .models import Ticket, Category, Comment

# registering the models to the admin panel
admin.site.register(Ticket)
admin.site.register(Category)
admin.site.register(Comment)
