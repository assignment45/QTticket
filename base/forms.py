from django.forms import ModelForm
from .models import Ticket

# ticket form
class TicketForm(ModelForm):
    class Meta:
        model = Ticket
        fields = '__all__'
        exclude = ['creator', 'members']