from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib import messages
from django.db.models import Q
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User

from .models import Ticket, Category, Comment
from .forms import TicketForm

# from django.views.decorators.csrf import csrf_exempt

## authentication/login ##
# login
def loginView(request):
    page = 'loginPage'
    if request.user.is_authenticated:
        return redirect('home')

    if request.method == 'POST':
        username = request.POST.get('username').lower()
        password = request.POST.get('password')

        try:
            user = User.objects.get(username=username)
            messages.success(request, 'Login Successful')
        except:
            messages.error(request, 'User does not exist, please register')

        user = authenticate(request, username=username, password=password)

        if user != None:
            login(request, user)
            return redirect('home')
        else:
            messages.warning(request, 'Username or password incorrect')

    context = {'page': page}
    return render(request, 'base/login.html', context)

# logout
def logoutView(request):
    logout(request)
    return redirect('home')

# register
def registerView(request):
    form = UserCreationForm()

    context = {'form': form}

    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.username = user.username.lower()
            user.save()
            login(request, user)
            messages.success(request, 'Registration Successful')
            return redirect('home')
        else:
            messages.error(request, 'Registration Unsuccessful')

    return render(request, 'base/login.html', context)

## pages ##
# homepage
def home(request):
    searchQuery = request.GET.get('searchQuery') if request.GET.get('searchQuery') != None else ''

    tickets = Ticket.objects.filter(
    Q(category__name__icontains=searchQuery) |
    Q(name__icontains=searchQuery)
    )

    categories = Category.objects.all()
    ticket_total = tickets.count() if tickets.count() else 'no'

    context = {'tickets': tickets, 'categories': categories, 'ticket_total': ticket_total}
    return render(request, 'base/home.html', context)

# profile page
def profile(request, pk):
    user = User.objects.get(id=pk)
    tickets = user.ticket_set.all()
    categories = Category.objects.all()

    context = {'user': user, 'tickets': tickets, 'categories': categories}
    return render(request, 'base/profile.html', context)

# ticket page
def ticket(request, pk):
    ticket = Ticket.objects.get(id=pk)
    members = ticket.members.all()
    comments = ticket.comment_set.all().order_by('-created') 
    staff = ""
    contributor = ""

    for user in members:
        if user != ticket.creator and not user.is_staff:
            contributor = user
        elif user.is_staff:
            staff = user

    if request.method == 'POST':
        comment = Comment.objects.create(
            user=request.user,
            ticket=ticket,
            body=request.POST.get('comment')
        )
        ticket.members.add(request.user)
        return redirect('ticket', pk=ticket.id)

    context = {'ticket': ticket, 'members': members, 'comments': comments, 'contributor': contributor, 'staff': staff}
    return render(request, 'base/ticket.html', context)

## ticket CRUD operations ##
# create ticket form 
@login_required(login_url='/login')
def createTicket(request):
    form = TicketForm()
    categories = Category.objects.all()

    if request.method == 'POST':
        category_name = request.POST.get('category')
        category, created = Category.objects.get_or_create(name=category_name)

        ticket= Ticket.objects.create(
            creator=request.user,
            category=category,
            name=request.POST.get('name'),
            description=request.POST.get('description'),
        )
        ticket.members.add(User.objects.get(is_staff=True))
        return redirect('home')

    context = {'form': form, 'categories': categories}
    return render(request, 'base/ticket_form.html', context)

# update ticket form
@login_required(login_url='/login')
def updateTicket(request, pk):
    ticket = Ticket.objects.get(id=pk)
    form = TicketForm(instance=ticket)
    categories = Category.objects.all()

    if request.user.is_staff == False:
        if request.user != ticket.creator:
            return HttpResponse('You do not have access to this page. Please contact the administrator if you believe there is an error.')

    if request.method == 'POST':
        category_name = request.POST.get('category')
        category, created = Category.objects.get_or_create(name=category_name)
        ticket.category = category
        ticket.name = request.POST.get('name')
        ticket.description = request.POST.get('description')
        ticket.save()
        return redirect('home')

    context = {'categories': categories, 'form': form, 'ticket': ticket}
    return render(request, 'base/ticket_form.html', context)

# delete/resolve ticket 
@login_required(login_url='/login')
def resolveTicket(request, pk):
    ticket = Ticket.objects.get(id=pk)

    if request.user.is_staff == False:
        if request.user != ticket.creator:
            return HttpResponse('You do not have access to this page. Please contact the administrator if you believe there is an error.')
        
    if request.method == 'POST':
        ticket.delete()
        return redirect('home')

    return render(request, 'base/resolve.html', {'obj': ticket})

# delete ticket comment
@login_required(login_url='/login')
def deleteComment(request, pk):
    comment = Comment.objects.get(id=pk)

    if request.user.is_staff == False:
        if request.user != comment.user:
            return HttpResponse('You do not have access to this page. Please contact the administrator if you believe there is an error.')

    if request.method == 'POST':
        comment.delete()
        return redirect('home')

    return render(request, 'base/resolve.html', {'obj': comment})    