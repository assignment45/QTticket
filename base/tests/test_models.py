from datetime import date
from django.test import TestCase
from base.models import Category, Ticket, Comment, User

## models testing ##
class CategoryModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        Category.objects.create(name="Test Category")

    def test_category_name_label(self):
        category = Category.objects.get(id=1)
        field_label = category._meta.get_field('name').verbose_name
        self.assertEqual(field_label, 'name')

    def test_category_name_max_length(self):
        category = Category.objects.get(id=1)
        max_length = category._meta.get_field('name').max_length
        self.assertEqual(max_length, 200)

class TicketModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        ticket = Ticket.objects.create(
            creator = User.objects.create(username="Test_User"),
            category = Category.objects.create(name="Test Category"),
            name = "Example ticket name",
            description = "Example ticket description.",
            updated = date.today(),
            created = date.today(),
            )
        ticket.members.add(User.objects.get(id=1))

    def test_ticket_creator_label(self):
        ticket = Ticket.objects.get(id=1)
        field_label = ticket._meta.get_field('creator').verbose_name
        self.assertEqual(field_label, 'creator')
    
    def test_ticket_category_label(self):
        ticket = Ticket.objects.get(id=1)
        field_label = ticket._meta.get_field('category').verbose_name
        self.assertEqual(field_label, 'category')

    def test_ticket_name_label(self):
        ticket = Ticket.objects.get(id=1)
        field_label = ticket._meta.get_field('name').verbose_name
        self.assertEqual(field_label, 'name')

    def test_description_creator_label(self):
        ticket = Ticket.objects.get(id=1)
        field_label = ticket._meta.get_field('description').verbose_name
        self.assertEqual(field_label, 'description')

    def test_ticket_updated_label(self):
        ticket = Ticket.objects.get(id=1)
        field_label = ticket._meta.get_field('updated').verbose_name
        self.assertEqual(field_label, 'updated')

    def test_ticket_created_label(self):
        ticket = Ticket.objects.get(id=1)
        field_label = ticket._meta.get_field('created').verbose_name
        self.assertEqual(field_label, 'created')

    def test_ticket_name_max_length(self):
        ticket = Ticket.objects.get(id=1)
        max_length = ticket._meta.get_field('name').max_length
        self.assertEqual(max_length, 200)
    
class CommentModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
         Comment.objects.create(
            user = User.objects.create(username="Test_User_1"),
            ticket = Ticket.objects.create(
            creator = User.objects.create(username="Test_User_2"),
            category = Category.objects.create(name="Test Category"),
            name = "Example ticket name",
            description = "Example ticket description.",
            updated = date.today(),
            created = date.today(),
            ),
            body = "Example comment body",
            )

    def test_comment_user_label(self):
        comment = Comment.objects.get(id=1)
        field_label = comment._meta.get_field('user').verbose_name
        self.assertEqual(field_label, 'user')
    
    def test_comment_ticket_label(self):
        comment = Comment.objects.get(id=1)
        field_label = comment._meta.get_field('ticket').verbose_name
        self.assertEqual(field_label, 'ticket')

    def test_comment_body_label(self):
        comment = Comment.objects.get(id=1)
        field_label = comment._meta.get_field('body').verbose_name
        self.assertEqual(field_label, 'body')


        