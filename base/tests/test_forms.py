import random
import time
from django.test import LiveServerTestCase, TestCase
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.keys import Keys
from base.forms import TicketForm

# automated End-To-End (E2E) tests of registration using Selenium 
class AutomatedFormTest(LiveServerTestCase):
  def test_register_form_password_match(self):

    chrome_options = Options()
    chrome_options.add_argument("--headless")
    selenium = webdriver.Chrome(executable_path=ChromeDriverManager().install(), options=chrome_options)
    selenium.get('http://localhost:8000/register')

    time.sleep(2)

    username = selenium.find_element_by_id('id_username')
    password = selenium.find_element_by_id('id_password1')
    password_confirm = selenium.find_element_by_id('id_password2')

    submit = selenium.find_element_by_id('id_submit')

    new_username = 'Test_User' + str(random.randint(1,100000))
    username.send_keys(new_username)
    password.send_keys('testPassword123')
    password_confirm.send_keys('testPassword123')

    submit.send_keys(Keys.RETURN)

    time.sleep(2)

    assert 'Registration Successful' in selenium.page_source
    selenium.close()

  def test_register_form_password_no_match(self):

    chrome_options = Options()
    chrome_options.add_argument("--headless")
    selenium = webdriver.Chrome(executable_path=ChromeDriverManager().install(), options=chrome_options)
    selenium.get('http://localhost:8000/register')

    time.sleep(2)

    username = selenium.find_element_by_id('id_username')
    password = selenium.find_element_by_id('id_password1')
    password_confirm = selenium.find_element_by_id('id_password2')

    submit = selenium.find_element_by_id('id_submit')

    username.send_keys('Test_User' + str(random.randint(1,100000)))
    password.send_keys('testPassword123')
    password_confirm.send_keys('testpassword')

    submit.send_keys(Keys.RETURN)

    time.sleep(2)

    assert 'Registration Unsuccessful' in selenium.page_source
    selenium.close()

# Unit test of the TicketForm instance
class TicketFormTest(TestCase):
    def setUp(self):
        TicketForm(data={
          "category": "test category", "name": "example name", "description": "example description"
          })

    def test_ticket_form_category_field_required(self):
        form = TicketForm()
        self.assertTrue(form.fields['category'].required)

    def test_ticket_form_category_field_help_text(self):
        form = TicketForm()
        self.assertEqual(form.fields['category'].help_text, 'Select or create a new category that best represents your query.')

    def test_ticket_form_name_field_required(self):
        form = TicketForm()
        self.assertTrue(form.fields['name'].required)

    def test_ticket_form_name_field_help_text(self):
        form = TicketForm()
        self.assertEqual(form.fields['name'].help_text, 'Briefly summarise your query in a maximum of 200 characters.')

    def test_ticket_form_description_field_required(self):
        form = TicketForm()
        self.assertTrue(form.fields['description'].required)

    def test_ticket_form_description_field_help_text(self):
        form = TicketForm()
        self.assertEqual(form.fields['description'].help_text, 'Please describe your query in greater detail, including any important environment information.')