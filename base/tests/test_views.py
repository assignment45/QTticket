from datetime import date
from django.test import TestCase
from django.urls import reverse
from django.contrib.auth.mixins import LoginRequiredMixin
from http import HTTPStatus

from base.models import Category, Ticket, Comment, User

class LoginViewTest(TestCase):
    
    def test_view_url_exists_at_desired_location(self):
        response = self.client.get('/login/')
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_view_url_accessible_by_name(self):
        response = self.client.get(reverse('login'))
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_view_uses_correct_template(self):
        response = self.client.get(reverse('login'))
        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertTemplateUsed(response, 'base/login.html')

class LogoutViewTest(TestCase):
    
    def test_view_url_exists_at_desired_location(self):
        response = self.client.get('/logout/')
        self.assertEqual(response.status_code, HTTPStatus.FOUND)

    def test_view_url_accessible_by_name(self):
        response = self.client.get(reverse('logout'))
        self.assertEqual(response.status_code, HTTPStatus.FOUND)

class RegisterViewTest(TestCase):
    
    def test_view_url_exists_at_desired_location(self):
        response = self.client.get('/register/')
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_view_url_accessible_by_name(self):
        response = self.client.get(reverse('register'))
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_view_uses_correct_template(self):
        response = self.client.get(reverse('register'))
        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertTemplateUsed(response, 'base/login.html')

class HomeViewTest(TestCase):
    
    def test_view_url_exists_at_desired_location(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_view_url_accessible_by_name(self):
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_view_uses_correct_template(self):
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertTemplateUsed(response, 'base/home.html')

class ProfileViewTest(TestCase):

    def test_view_url_accessible_by_name(self):
        User.objects.create(username="test_user")
        response = self.client.get(reverse('profile', args=[1]))
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_view_uses_correct_template(self):
        User.objects.create(username="test_user")
        response = self.client.get(reverse('profile', args=[1]))
        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertTemplateUsed(response, 'base/profile.html')

class TicketViewTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        ticket = Ticket.objects.create(
            creator = User.objects.create(username="Test_User"),
            category = Category.objects.create(name="Test Category"),
            name = "Example ticket name",
            description = "Example ticket description.",
            updated = date.today(),
            created = date.today(),
            )
        ticket.members.add(User.objects.get(id=1))

    def test_view_url_accessible_by_name(self):
        response = self.client.get(reverse('ticket', args=[1]))
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_view_uses_correct_template(self):
        response = self.client.get(reverse('ticket', args=[1]))
        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertTemplateUsed(response, 'base/ticket.html')

# ticket create form integration tests

class CreateTicketViewTests(TestCase, LoginRequiredMixin):
    def test_get(self):
        response = self.client.get("/create-ticket/")

        self.assertEqual(response.status_code, HTTPStatus.FOUND)

    def test_post_success(self):
        response = self.client.post("/create-ticket/", data={
            "category": "Test Category",
            "name": "Example Name",
            "description": "Example Description"
            })

        self.assertEqual(response.status_code, HTTPStatus.FOUND)

    def test_post_error(self):
        response = self.client.post("/create-ticket/", data={
            "test": "test",
            })

        self.assertEqual(response.status_code, HTTPStatus.FOUND)