from django.db import models
from django.contrib.auth.models import User

## database models ##
# category model
class Category(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name

# ticket model
class Ticket(models.Model):
    creator = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)
    members = models.ManyToManyField(User, related_name='members', blank=True)
    category = models.ForeignKey(Category, null=True, on_delete=models.SET_NULL, help_text="Select or create a new category that best represents your query.")
    name = models.CharField(max_length=200, help_text="Briefly summarise your query in a maximum of 200 characters.")
    description = models.TextField(help_text="Please describe your query in greater detail, including any important environment information.")
    updated = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-updated', '-created']

    def __str__(self):
        return '{} {}'.format(self.name, self.description)

# comment model
class Comment(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)  #one to many
    ticket = models.ForeignKey(Ticket, on_delete=models.CASCADE) #many to one
    body = models.TextField()
    updated = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.body[0:20]