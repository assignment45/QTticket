# QTticket

QTticket is a ticket management system for Human Resource teams written in Python, using the Django web framework.

![QTticket homepage](/static/homepage.png "QTticket homepage")

## Demo Site

View a demo/playground site [here](https://qt-ticket.herokuapp.com), or go to
[qt-ticket.herokuapp.com](https://qt-ticket.herokuapp.com)

Feel free to register for an account.

To test the admin functionality please login with:

**Username:** admin

**Password:** !sNE,W_mX`Dkb5pL

## Installation

Want to run the app locally?

It is recommended you use a virtual environment such as [virtualenv](https://virtualenv.pypa.io/en/latest/installation.html).

Once inside the virtual environment, navigate to the project and use the package manager [pip](https://pip.pypa.io/en/stable/) to run:

```bash
pip install -r requirements.txt
```

This will install all required modules.

## Usage

After installing the modules, run the following commands from the root directory.

Run project:
```bash
python manage.py runserver
```

Run tests:
```bash
python manage.py test
```
