//Getting dropdownMenu and dropdownButton from the DOM
const dropdownMenu = document.querySelector(".dropdown-menu");
const dropdownButton = document.querySelector(".dropdown-button");

//When dropdown button is clicked, the menu is shown
if (dropdownButton) {
  document.addEventListener("click", (event) => {
    const clickedButton = event.composedPath().includes(dropdownButton);
    //checks if button is clicked, if clicked outside with dropdown menu open, it will close
    if (clickedButton) {
      dropdownMenu.classList.toggle("show");
    } else if (dropdownMenu.className == "dropdown-menu show") {
      dropdownMenu.classList.toggle("show");
      dropdownButton.classList.toggle("active");
    }
  });
}
